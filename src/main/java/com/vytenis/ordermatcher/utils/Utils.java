package com.vytenis.ordermatcher.utils;

import com.vytenis.ordermatcher.domain.models.Order;

import java.util.Arrays;
import java.util.List;

public class Utils {

    public static List<String> NO_ARG_COMMANDS = Arrays.asList("PRINT");

    public static int sortBuyOrders(Order o1, Order o2) {
        if (o2.getPrice() == o1.getPrice()) {
            return Integer.compare(o1.getId(), o2.getId());
        } else {
            return Integer.compare(o2.getPrice(), o1.getPrice());
        }
    }

    public static int sortSellOrders(Order o1, Order o2) {
        if (o1.getPrice() == o2.getPrice()) {
            return Integer.compare(o1.getId(), o2.getId());
        } else {
            return Integer.compare(o1.getPrice(), o2.getPrice());
        }
    }
}
