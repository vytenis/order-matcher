package com.vytenis.ordermatcher.domain.datastore;

import com.vytenis.ordermatcher.domain.models.BuyOrder;
import com.vytenis.ordermatcher.domain.models.SellOrder;

import java.util.ArrayList;
import java.util.List;

public class DummyDataStore {
    private List<BuyOrder> allBuyOrders = new ArrayList<>();
    private List<SellOrder> allSellOrders = new ArrayList<>();

    private static DummyDataStore dummyDataStore = new DummyDataStore();

    private DummyDataStore() {

    }

    public static DummyDataStore getInstance() {
        return dummyDataStore;
    }

    public List<BuyOrder> getAllBuyOrders() {
        return allBuyOrders;
    }

    public List<SellOrder> getAllSellOrders() {
        return allSellOrders;
    }
}
