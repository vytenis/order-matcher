package com.vytenis.ordermatcher.domain.repositories;

import com.vytenis.ordermatcher.domain.datastore.DummyDataStore;
import com.vytenis.ordermatcher.domain.models.BuyOrder;

import java.util.List;

public class BuyOrderRepository {
    private DummyDataStore dataStore = DummyDataStore.getInstance();

    private static BuyOrderRepository buyOrderRepository = new BuyOrderRepository();

    private BuyOrderRepository() {

    }

    public static BuyOrderRepository getInstance() {
        return buyOrderRepository;
    }

    public List<BuyOrder> getAllBuyOrders() {
        return this.dataStore.getAllBuyOrders();
    }

    public void addBuyOrder(BuyOrder buyOrder) {
        dataStore.getAllBuyOrders().add(buyOrder);
    }

    public void removeBuyOrders(List<BuyOrder> buyOrders) {
        dataStore.getAllBuyOrders().removeAll(buyOrders);
    }
}
