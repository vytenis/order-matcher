package com.vytenis.ordermatcher.domain.repositories;

import com.vytenis.ordermatcher.domain.datastore.DummyDataStore;
import com.vytenis.ordermatcher.domain.models.SellOrder;

import java.util.List;

public class SellOrderRepository {
    DummyDataStore dataStore = DummyDataStore.getInstance();

    private static SellOrderRepository sellOrderRepository = new SellOrderRepository();

    private SellOrderRepository() {

    }

    public static SellOrderRepository getInstance() {
        return sellOrderRepository;
    }

    public List<SellOrder> getAllSellOrders() {
        return this.dataStore.getAllSellOrders();
    }

    public void addSellOrder(SellOrder sellOrder) {
        dataStore.getAllSellOrders().add(sellOrder);
    }

    public void removeSellOrders(List<SellOrder> sellOrders) {
        dataStore.getAllSellOrders().removeAll(sellOrders);
    }

}
