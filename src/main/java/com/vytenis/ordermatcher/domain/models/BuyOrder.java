package com.vytenis.ordermatcher.domain.models;

public class BuyOrder extends Order {

    public BuyOrder(int volume, int price) {
        super(volume, price);
    }

    @Override
    public String toString() {
        return "BUY " + super.getVolume() +
                "@" + super.getPrice();
    }
}
