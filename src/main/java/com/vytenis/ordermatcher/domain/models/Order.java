package com.vytenis.ordermatcher.domain.models;

import java.util.concurrent.atomic.AtomicInteger;

public abstract class Order {
    private static final AtomicInteger count = new AtomicInteger(0);
    private int Id;
    private int volume;
    private int price;

    public Order(int volume, int price) {
        this.Id = count.incrementAndGet();
        this.volume = volume;
        this.price = price;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public int getPrice() {
        return price;
    }

    public int getId() {
        return Id;
    }

    public enum OrderType {
        BUY, SELL
    }

}
