package com.vytenis.ordermatcher.domain.models;

public class SellOrder extends Order {

    public SellOrder(int volume, int price) {
        super(volume, price);
    }

    @Override
    public String toString() {
        return "SELL " + super.getVolume() +
                "@" + super.getPrice();
    }
}
