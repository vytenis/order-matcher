package com.vytenis.ordermatcher.domain.models;

public class Trade extends Order {

    public Trade(int volume, int price) {
        super(volume, price);
    }

    @Override
    public String toString() {
        return "TRADE " + super.getVolume() +
                "@" + super.getPrice();
    }
}
