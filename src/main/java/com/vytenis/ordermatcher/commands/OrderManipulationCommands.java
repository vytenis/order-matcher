package com.vytenis.ordermatcher.commands;

import com.vytenis.ordermatcher.converters.CommandConverter;
import com.vytenis.ordermatcher.domain.models.BuyOrder;
import com.vytenis.ordermatcher.domain.models.Order;
import com.vytenis.ordermatcher.domain.models.SellOrder;
import com.vytenis.ordermatcher.domain.models.Trade;
import com.vytenis.ordermatcher.domain.repositories.BuyOrderRepository;
import com.vytenis.ordermatcher.domain.repositories.SellOrderRepository;
import com.vytenis.ordermatcher.services.OrderMatchingService;

import java.util.List;

public class OrderManipulationCommands {

    private static OrderManipulationCommands orderManipulationCommands = new OrderManipulationCommands();
    private OrderMatchingService orderMatchingService = OrderMatchingService.getInstance();
    private BuyOrderRepository buyOrderRepository = BuyOrderRepository.getInstance();
    private SellOrderRepository sellOrderRepository = SellOrderRepository.getInstance();

    private OrderManipulationCommands() {

    }

    public static OrderManipulationCommands getInstance() {
        return orderManipulationCommands;
    }

    public void buy(String pattern) {
        Order order = CommandConverter.generateOrder(pattern, Order.OrderType.BUY);
        buyOrderRepository.addBuyOrder((BuyOrder) order);
        List<Trade> executedTrades = orderMatchingService.executeMatching(order, Order.OrderType.SELL);
        executedTrades.forEach(executedTrade -> System.out.println(executedTrade.toString()));
    }

    public void sell(String pattern) {
        Order order = CommandConverter.generateOrder(pattern, Order.OrderType.SELL);
        sellOrderRepository.addSellOrder((SellOrder) order);
        List<Trade> executedTrades = orderMatchingService.executeMatching(order, Order.OrderType.BUY);
        executedTrades.forEach(executedTrade -> System.out.println(executedTrade.toString()));
    }
}
