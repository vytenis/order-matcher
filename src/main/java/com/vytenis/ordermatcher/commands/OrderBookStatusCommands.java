package com.vytenis.ordermatcher.commands;

import com.vytenis.ordermatcher.domain.repositories.BuyOrderRepository;
import com.vytenis.ordermatcher.domain.repositories.SellOrderRepository;
import com.vytenis.ordermatcher.utils.Utils;

import java.util.stream.Collectors;

public class OrderBookStatusCommands {

    private BuyOrderRepository buyOrderRepository = BuyOrderRepository.getInstance();
    private SellOrderRepository sellOrderRepository = SellOrderRepository.getInstance();

    private static OrderBookStatusCommands orderBookStatusCommands = new OrderBookStatusCommands();

    private OrderBookStatusCommands() {

    }

    public static OrderBookStatusCommands getInstance() {
        return orderBookStatusCommands;
    }

    public String print() {
        StringBuilder sb = new StringBuilder();
        sb.append("--- SELL ---\n");
        sb.append(getListOfSellOrders());
        sb.append("\n--- BUY ---\n");
        sb.append(getListOfBuyOrders());
        return sb.toString();
    }

    private String getListOfSellOrders() {
        return sellOrderRepository.getAllSellOrders().stream()
                .sorted(Utils::sortSellOrders)
                .map(Object::toString)
                .collect(Collectors.joining("\n"));
    }

    private String getListOfBuyOrders() {
        return buyOrderRepository.getAllBuyOrders().stream()
                .sorted(Utils::sortBuyOrders)
                .map(Object::toString)
                .collect(Collectors.joining("\n"));
    }
}
