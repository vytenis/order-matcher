package com.vytenis.ordermatcher.services;

import com.vytenis.ordermatcher.domain.models.BuyOrder;
import com.vytenis.ordermatcher.domain.models.Order;
import com.vytenis.ordermatcher.domain.models.SellOrder;
import com.vytenis.ordermatcher.domain.models.Trade;
import com.vytenis.ordermatcher.domain.repositories.BuyOrderRepository;
import com.vytenis.ordermatcher.domain.repositories.SellOrderRepository;
import com.vytenis.ordermatcher.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class OrderMatchingService {

    private static OrderMatchingService orderMatchingService = new OrderMatchingService();
    private BuyOrderRepository buyOrderRepository = BuyOrderRepository.getInstance();
    private SellOrderRepository sellOrderRepository = SellOrderRepository.getInstance();


    private OrderMatchingService() {

    }

    public static OrderMatchingService getInstance() {
        return orderMatchingService;
    }

    public List<Trade> executeMatching(Order order, Order.OrderType orderTypeToMatch) {
        List<Order> allMatchingOrders = getMatchingOrders(order, orderTypeToMatch);
        List<Trade> executedTrades = executeTrades(order, allMatchingOrders);
        cleanFilledOrders();
        return executedTrades;
    }

    private List<Order> getMatchingOrders(Order order, Order.OrderType orderTypeToMatch) {

        List<Order> matchingOrders = Collections.emptyList();

        switch (orderTypeToMatch) {
            case SELL:
                matchingOrders = sellOrderRepository.getAllSellOrders().stream()
                        .filter(sellOrder -> sellOrder.getPrice() <= order.getPrice())
                        .sorted(Utils::sortSellOrders)
                        .collect(Collectors.toList());
                break;
            case BUY:
                matchingOrders = buyOrderRepository.getAllBuyOrders().stream()
                        .filter(buyOrder -> buyOrder.getPrice() >= order.getPrice())
                        .sorted(Utils::sortBuyOrders)
                        .collect(Collectors.toList());
                break;
        }
        return matchingOrders;
    }

    private List<Trade> executeTrades(Order newlyEnteredOrder, List<Order> allMatchingOrders) {
        List<Trade> executedTrades = new ArrayList<>();

        for (Order matchingOrder : allMatchingOrders) {

            if (newlyEnteredOrder.getVolume() == 0) {
                break;
            }

            int volumeToDecrease = newlyEnteredOrder.getVolume() >= matchingOrder.getVolume() ? matchingOrder.getVolume() : newlyEnteredOrder.getVolume();
            newlyEnteredOrder.setVolume(newlyEnteredOrder.getVolume() - volumeToDecrease);
            matchingOrder.setVolume(matchingOrder.getVolume() - volumeToDecrease);

            executedTrades.add(new Trade(volumeToDecrease, matchingOrder.getPrice()));
        }

        return executedTrades;
    }

    private void cleanFilledOrders() {
        List<BuyOrder> buyOrdersToRemove = buyOrderRepository.getAllBuyOrders().stream()
                .filter(buyOrder -> buyOrder.getVolume() == 0)
                .collect(Collectors.toList());

        List<SellOrder> sellOrdersToRemove = sellOrderRepository.getAllSellOrders().stream()
                .filter(sellOrder -> sellOrder.getVolume() == 0)
                .collect(Collectors.toList());

        buyOrderRepository.removeBuyOrders(buyOrdersToRemove);
        sellOrderRepository.removeSellOrders(sellOrdersToRemove);
    }
}
