package com.vytenis.ordermatcher.converters;

import com.vytenis.ordermatcher.domain.models.BuyOrder;
import com.vytenis.ordermatcher.domain.models.Order;
import com.vytenis.ordermatcher.domain.models.SellOrder;
import com.vytenis.ordermatcher.utils.Utils;

public class CommandConverter {

    private static final String INVALID_VOLUME_OR_AMOUNT = "Invalid volume or amount. Negative or zero value are not allowed";
    private static final String INVALID_ORDER_TYPE = "Invalid order type";
    private static final String FAILED_TO_PARSE_ORDER = "Failed to parse order. Please check the input format";
    private static final String SPLIT_CHAR = "@";
    private static final String SYMBOL_INSTEAD_OF_NUMBER = "You seem to have entered symbol instead of number. You still have the chance to fix that ;)";

    public static Order generateOrder(String orderString, Order.OrderType orderType) {
        try {
            String[] parsedOrder = orderString.split(SPLIT_CHAR);

            int orderVolume = Integer.parseInt(parsedOrder[0]);
            int orderPrice = Integer.parseInt(parsedOrder[1]);

            if (!isIntegerCorrect(orderVolume) || !isIntegerCorrect(orderPrice)) {
                throw new IllegalArgumentException(INVALID_VOLUME_OR_AMOUNT);
            }

            return getOrder(orderType, orderVolume, orderPrice);

        } catch (IllegalArgumentException | ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException(SYMBOL_INSTEAD_OF_NUMBER);
        } catch (RuntimeException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private static Order getOrder(Order.OrderType orderType, int orderVolume, int orderPrice) {
        switch (orderType) {
            case BUY:
                return new BuyOrder(orderVolume, orderPrice);
            case SELL:
                return new SellOrder(orderVolume, orderPrice);
            default:
                throw new IllegalArgumentException(INVALID_ORDER_TYPE);
        }
    }

    private static boolean isIntegerCorrect(int value) {
        return value > 0;
    }

    public static String[] getAction(String input) {
        String[] parsedInput = input.split(" ", 2);

        if (!input.equals("") && parsedInput.length != 2 && !Utils.NO_ARG_COMMANDS.contains(parsedInput[0])) {
            throw new IllegalArgumentException(FAILED_TO_PARSE_ORDER);
        }

        return parsedInput;
    }
}
