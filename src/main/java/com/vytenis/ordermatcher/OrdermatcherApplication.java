package com.vytenis.ordermatcher;

import com.vytenis.ordermatcher.commands.OrderBookStatusCommands;
import com.vytenis.ordermatcher.commands.OrderManipulationCommands;
import com.vytenis.ordermatcher.converters.CommandConverter;

import java.util.Scanner;

public class OrdermatcherApplication {

    private static final String AVAILABLE_COMMANDS = "Enter one of the following commands: BUY, SELL, PRINT, EXIT";
    private static final String COMMAND_WAS_NOT_RECOGNISED = "Command was not recognised. Please try again";
    private static final String EXIT = "EXIT";
    private static final String BUY = "BUY";
    private static final String SELL = "SELL";
    private static final String PRINT = "PRINT";

    public static void main(String[] args) {

        OrderManipulationCommands orderManipulationCommands = OrderManipulationCommands.getInstance();
        OrderBookStatusCommands orderBookStatusCommands = OrderBookStatusCommands.getInstance();
        String input;

        System.out.println(AVAILABLE_COMMANDS);
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print(">");
            input = scanner.nextLine();

            if(input.equals(EXIT)) {
                break;
            }

            try {
                String[] inputData = CommandConverter.getAction(input);
                switch (inputData[0]) {
                    case BUY:
                        orderManipulationCommands.buy(inputData[1]);
                        break;
                    case SELL:
                        orderManipulationCommands.sell(inputData[1]);
                        break;
                    case PRINT:
                        System.out.println(orderBookStatusCommands.print());
                        break;
                    default:
                        System.out.println(COMMAND_WAS_NOT_RECOGNISED);
                        break;
                }
            } catch (RuntimeException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
