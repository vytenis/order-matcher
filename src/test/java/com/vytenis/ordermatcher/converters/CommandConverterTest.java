package com.vytenis.ordermatcher.converters;

import com.vytenis.ordermatcher.domain.models.Order;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CommandConverterTest {

    @Test
    public void givenValidPattern_whenConvertIsCalled_thenPatternIsParsed() {
        Order order = CommandConverter.generateOrder("100@50", Order.OrderType.BUY);

        assertEquals(order.getPrice(), 50);
        assertEquals(order.getVolume(), 100);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenNotAllowedValues_whenConvertIsCalled_thenExceptionIsThrown() {
        Order order = CommandConverter.generateOrder("-100@50", Order.OrderType.BUY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenInvalidPattern_whenConvertIsCalled_thenExceptionIsThrown() {
        CommandConverter.generateOrder("100@@@", Order.OrderType.BUY);
    }
}