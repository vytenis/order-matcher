package com.vytenis.ordermatcher.services;

import com.vytenis.ordermatcher.domain.models.BuyOrder;
import com.vytenis.ordermatcher.domain.models.Order;
import com.vytenis.ordermatcher.domain.models.SellOrder;
import com.vytenis.ordermatcher.domain.repositories.BuyOrderRepository;
import com.vytenis.ordermatcher.domain.repositories.SellOrderRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class OrderMatchingServiceTest {

    BuyOrderRepository buyOrderRepositoryMock;
    SellOrderRepository sellOrderRepositoryMock;
    private OrderMatchingService orderMatchingService;

    @Before
    public void setUp() {
        List<BuyOrder> allBuyOrders = new ArrayList<>();
        List<SellOrder> allSellOrders = new ArrayList<>();
        buyOrderRepositoryMock = BuyOrderRepository.getInstance();
        sellOrderRepositoryMock = SellOrderRepository.getInstance();
        orderMatchingService = OrderMatchingService.getInstance();
    }

    @After
    public void tearDown() {
        buyOrderRepositoryMock.getAllBuyOrders().clear();
        sellOrderRepositoryMock.getAllSellOrders().clear();
    }

    @Test
    public void givenNoOrdersInRepo_whenMatchingExecuted_thenNoOrdersMathed() {
        BuyOrder buyOrder1 = new BuyOrder(100, 90);

        assert (orderMatchingService.executeMatching(buyOrder1, Order.OrderType.SELL).size() == 0);
        assert (buyOrder1.getVolume() == 100);
    }

    @Test
    public void givenOneMatchingOrderInRepo_whenMatchingExecuted_thenOneOrderMatched() {
        BuyOrder buyOrder1 = new BuyOrder(100, 90);
        sellOrderRepositoryMock.addSellOrder(new SellOrder(50, 50));

        assert (orderMatchingService.executeMatching(buyOrder1, Order.OrderType.SELL).size() == 1);
        assert (sellOrderRepositoryMock.getAllSellOrders().size() == 0);
        assert (buyOrder1.getVolume() == 50);
    }

    @Test
    public void givenThreeMatchingOrdersInRepo_whenMatchingExecuted_thenAllOrdersMatched() {
        BuyOrder buyOrder1 = new BuyOrder(120, 90);

        sellOrderRepositoryMock.addSellOrder(new SellOrder(50, 50));
        sellOrderRepositoryMock.addSellOrder(new SellOrder(50, 55));
        sellOrderRepositoryMock.addSellOrder(new SellOrder(50, 60));

        assert (orderMatchingService.executeMatching(buyOrder1, Order.OrderType.SELL).size() == 3);
        assert (sellOrderRepositoryMock.getAllSellOrders().get(0).getVolume() == 30);
        assert (buyOrder1.getVolume() == 0);
    }
}