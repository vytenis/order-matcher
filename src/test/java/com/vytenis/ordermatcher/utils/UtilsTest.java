package com.vytenis.ordermatcher.utils;

import com.vytenis.ordermatcher.domain.models.BuyOrder;
import com.vytenis.ordermatcher.domain.models.SellOrder;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class UtilsTest {

    @Test
    public void givenBuyOrders_whenSortIsCalled_thenBuyOrdersAreSortedDescByPrice() {
        BuyOrder buyOrder1 = new BuyOrder(100, 90);
        BuyOrder buyOrder2 = new BuyOrder(100, 70);
        BuyOrder buyOrder3 = new BuyOrder(100, 80);

        List<BuyOrder> buyOrders = new ArrayList<>();
        buyOrders.add(buyOrder1);
        buyOrders.add(buyOrder2);
        buyOrders.add(buyOrder3);

        buyOrders.sort(Utils::sortBuyOrders);

        assertEquals(buyOrders.get(0).getPrice(), 90);
        assertEquals(buyOrders.get(2).getPrice(), 70);
    }

    @Test
    public void givenSellOrders_whenSortIsCalled_thenSellOrdersAreSortedAscByPrice() {
        SellOrder sellOrder1 = new SellOrder(100, 90);
        SellOrder sellOrder2 = new SellOrder(100, 70);
        SellOrder sellOrder3 = new SellOrder(100, 80);

        List<SellOrder> sellOrders = new ArrayList<>();
        sellOrders.add(sellOrder1);
        sellOrders.add(sellOrder2);
        sellOrders.add(sellOrder3);

        sellOrders.sort(Utils::sortSellOrders);

        assertEquals(sellOrders.get(0).getPrice(), 70);
        assertEquals(sellOrders.get(2).getPrice(), 90);
    }
}