HOW TO:  

* Clone repository
* Navigate to the root of the project
* Execute ```javac -cp .:lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar: -d bin @sources.txt``` from your terminal to compile the sources
* Execute ```java -cp bin com.vytenis.ordermatcher.OrdermatcherApplication``` from your terminal to launch the app
* To run tests execute the following:
* *  ```java -cp .:lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar:bin org.junit.runner.JUnitCore com.vytenis.ordermatcher.converters.CommandConverterTest```
* *  ```java -cp .:lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar:bin org.junit.runner.JUnitCore com.vytenis.ordermatcher.services.OrderMatchingServiceTest```
* *  ```java -cp .:lib/junit-4.12.jar:lib/hamcrest-core-1.3.jar:bin org.junit.runner.JUnitCore com.vytenis.ordermatcher.utils.UtilsTest```

This should be sufficient to get up and running

ADDITIONAL NOTES:  

* The only external dependency left is on Junit.
* I noticed that assignment said that trade orders should be in format of ```TRADE price@volume```, though all other examples had it like ```TRADE volume@price```, therefore for the sake of consistency I kept it ```TRADE price@volume```.
* Java 1.8.0_162 was used to compile the project.